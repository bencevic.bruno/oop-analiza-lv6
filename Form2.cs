ï»¿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace analiza
{
    public partial class Form2 : Form
    {
        String word = "";
        String wordGuessed = "";
        List<bool> guessed = new List<bool>();
        int guessesLeft = 9;

        /**
         * Objasnjenje koda:
         * Pri pokretanju se iz datoteke izvuce nasumicna rijec koja ce se pogadati
         * te se broj preostalih pokusaja postavlja na 9 na zaslonu.
         * 
         * Pri svakom pokusaju pogadanja:
         *      ako nije umetnuto samo jedno slovo, pokusaj se ne broji
         *      ako se ne pogodi, javi se user-u te smanji broj pokusaja
         *      ako se pogodi, na zaslon se ispiÅ¡u pogoÄena slova (tome sluÅ¾i lista guessed za ispis rijeci npr. zemlja ovako: z__lja, u funkciji generateWordLabel()) 
         *          te se provjerava jesu li sva slova pogodena (funkcija checkVictory())
         */
        public Form2()
        {
            InitializeComponent();
            loadRandomWord();
            label_movesLeft.Text = "Moves left: " + guessesLeft;
        }

        private void loadRandomWord()
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader("words.txt"))
            {
                List<String> words = new List<String>();
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    words.Add(line);
                }
                word = words[(new Random()).Next(words.Count)];

                for (int i = 0; i < word.Length; i++) guessed.Add(false);
            }
        }

        private void generateWordLabel()
        {
            wordGuessed = "";
            for (int i = 0; i < word.Length; i++)
            {
                if (guessed[i]) wordGuessed += (word[i] + " ");
                else wordGuessed += "_ ";
            }
        }
        
        private void checkVictory()
        {
            for(int i = 0; i < guessed.Count; i++)
            {
                if (!guessed[i])
                {
                    return;
                }
            }
            MessageBox.Show("Victory!", "Congrats!");
        }

        private void button_check_Click(object sender, EventArgs e)
        {
            if (guessesLeft == 0) MessageBox.Show("The word was: " + word, "The End");
            else if (textBox_guess.Text.Length != 1)
            {
                MessageBox.Show("You can only guess one letter at a time", "Error");
            }
            else
            {
                char letter = textBox_guess.Text[0];
                if (word.Contains(letter))
                {
                    for (int i = 0; i < word.Length; i++)
                    {
                        if (word[i] == letter)
                        {
                            guessed[i] = true;
                        }
                    }
                    generateWordLabel();
                    label_gussedLetters.Text = wordGuessed;

                    checkVictory();
                }
                else
                {
                    guessesLeft--;
                    label_movesLeft.Text = "Moves left: " + guessesLeft;
                    MessageBox.Show("Guess again!", "Wrong guess!");
                }
            }
        }
    }
}
