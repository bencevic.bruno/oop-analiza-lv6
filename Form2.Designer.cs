ï»¿namespace analiza
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_movesLeft = new System.Windows.Forms.Label();
            this.textBox_guess = new System.Windows.Forms.TextBox();
            this.button_check = new System.Windows.Forms.Button();
            this.label_gussedLetters = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_movesLeft
            // 
            this.label_movesLeft.AutoSize = true;
            this.label_movesLeft.Location = new System.Drawing.Point(14, 22);
            this.label_movesLeft.Name = "label_movesLeft";
            this.label_movesLeft.Size = new System.Drawing.Size(95, 21);
            this.label_movesLeft.TabIndex = 0;
            this.label_movesLeft.Text = "Moves left:";
            // 
            // textBox_guess
            // 
            this.textBox_guess.Location = new System.Drawing.Point(59, 90);
            this.textBox_guess.Name = "textBox_guess";
            this.textBox_guess.Size = new System.Drawing.Size(81, 22);
            this.textBox_guess.TabIndex = 1;
            // 
            // button_check
            // 
            this.button_check.Location = new System.Drawing.Point(182, 90);
            this.button_check.Name = "button_check";
            this.button_check.Size = new System.Drawing.Size(101, 27);
            this.button_check.TabIndex = 2;
            this.button_check.Text = "Check";
            this.button_check.UseVisualStyleBackColor = true;
            this.button_check.Click += new System.EventHandler(this.button_check_Click);
            // 
            // label_gussedLetters
            // 
            this.label_gussedLetters.AutoSize = true;
            this.label_gussedLetters.Location = new System.Drawing.Point(72, 56);
            this.label_gussedLetters.Name = "label_gussedLetters";
            this.label_gussedLetters.Size = new System.Drawing.Size(0, 21);
            this.label_gussedLetters.TabIndex = 3;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 145);
            this.Controls.Add(this.label_gussedLetters);
            this.Controls.Add(this.button_check);
            this.Controls.Add(this.textBox_guess);
            this.Controls.Add(this.label_movesLeft);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_movesLeft;
        private System.Windows.Forms.TextBox textBox_guess;
        private System.Windows.Forms.Button button_check;
        private System.Windows.Forms.Label label_gussedLetters;
    }
}