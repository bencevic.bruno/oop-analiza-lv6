ï»¿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace analiza
{
    public partial class Form1 : Form
    {
        private double op1, op2;
        public Form1()
        {
            InitializeComponent();
        }

        private bool checkOperands()
        {
            if (!double.TryParse(TextBox_op1.Text, out op1))
            {
                MessageBox.Show("Operand #1: not a number!", "Number Error");
                Label_result.Text = "Error";
                return false;
            }
            else if (!double.TryParse(TextBox_op2.Text, out op2))
            {
                MessageBox.Show("Operand 21: not a number!", "Number Error");
                Label_result.Text = "Error";
                return false;
            }
            return true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button_plus_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = (op1 + op2).ToString();
            }
        }

        private void button_minus_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = (op1 - op2).ToString();
            }
        }

        private void button_multiply_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = (op1 * op2).ToString();
            }
        }

        private void button_divide_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = (op1 / op2).ToString();
            }
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = "#1: " + Math.Sin(op1) + "\n#2: " + Math.Sin(op2);
            }
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = "#1: " + Math.Cos(op1) + "\n#2: " + Math.Cos(op2);
            }
        }

        private void button_tan_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = "#1: " + Math.Tan(op1) + "\n#2: " + Math.Tan(op2);
            }
        }

        private void button_ln_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = "#1: " + Math.Log(op1) + "\n#2: " + Math.Log(op2);
            }
        }

        private void button_sqrt_Click(object sender, EventArgs e)
        {
            if (checkOperands())
            {
                Label_result.Text = "#1: " + Math.Sqrt(op1) + "\n#2: " + Math.Sqrt(op2);
            }
        }

        private void TextBox_op1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
