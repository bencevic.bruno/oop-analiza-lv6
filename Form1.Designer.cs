ï»¿namespace analiza
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBox_op1 = new System.Windows.Forms.TextBox();
            this.TextBox_op2 = new System.Windows.Forms.TextBox();
            this.button_plus = new System.Windows.Forms.Button();
            this.button_minus = new System.Windows.Forms.Button();
            this.button_divide = new System.Windows.Forms.Button();
            this.button_sin = new System.Windows.Forms.Button();
            this.button_tan = new System.Windows.Forms.Button();
            this.button_ln = new System.Windows.Forms.Button();
            this.button_multiply = new System.Windows.Forms.Button();
            this.button_cos = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Label_result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Operand #1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Operand #2";
            // 
            // TextBox_op1
            // 
            this.TextBox_op1.Location = new System.Drawing.Point(102, 18);
            this.TextBox_op1.Name = "TextBox_op1";
            this.TextBox_op1.Size = new System.Drawing.Size(108, 22);
            this.TextBox_op1.TabIndex = 2;
            this.TextBox_op1.TextChanged += new System.EventHandler(this.TextBox_op1_TextChanged);
            // 
            // TextBox_op2
            // 
            this.TextBox_op2.Location = new System.Drawing.Point(102, 46);
            this.TextBox_op2.Name = "TextBox_op2";
            this.TextBox_op2.Size = new System.Drawing.Size(108, 22);
            this.TextBox_op2.TabIndex = 3;
            // 
            // button_plus
            // 
            this.button_plus.Location = new System.Drawing.Point(239, 28);
            this.button_plus.Name = "button_plus";
            this.button_plus.Size = new System.Drawing.Size(100, 31);
            this.button_plus.TabIndex = 4;
            this.button_plus.Text = "+";
            this.button_plus.UseVisualStyleBackColor = true;
            this.button_plus.Click += new System.EventHandler(this.button_plus_Click);
            // 
            // button_minus
            // 
            this.button_minus.Location = new System.Drawing.Point(239, 65);
            this.button_minus.Name = "button_minus";
            this.button_minus.Size = new System.Drawing.Size(100, 31);
            this.button_minus.TabIndex = 5;
            this.button_minus.Text = "-";
            this.button_minus.UseVisualStyleBackColor = true;
            this.button_minus.Click += new System.EventHandler(this.button_minus_Click);
            // 
            // button_divide
            // 
            this.button_divide.Location = new System.Drawing.Point(345, 28);
            this.button_divide.Name = "button_divide";
            this.button_divide.Size = new System.Drawing.Size(100, 31);
            this.button_divide.TabIndex = 6;
            this.button_divide.Text = "/";
            this.button_divide.UseVisualStyleBackColor = true;
            this.button_divide.Click += new System.EventHandler(this.button_divide_Click);
            // 
            // button_sin
            // 
            this.button_sin.Location = new System.Drawing.Point(345, 65);
            this.button_sin.Name = "button_sin";
            this.button_sin.Size = new System.Drawing.Size(100, 31);
            this.button_sin.TabIndex = 7;
            this.button_sin.Text = "sin";
            this.button_sin.UseVisualStyleBackColor = true;
            this.button_sin.Click += new System.EventHandler(this.button_sin_Click);
            // 
            // button_tan
            // 
            this.button_tan.Location = new System.Drawing.Point(451, 28);
            this.button_tan.Name = "button_tan";
            this.button_tan.Size = new System.Drawing.Size(100, 31);
            this.button_tan.TabIndex = 8;
            this.button_tan.Text = "tan";
            this.button_tan.UseVisualStyleBackColor = true;
            this.button_tan.Click += new System.EventHandler(this.button_tan_Click);
            // 
            // button_ln
            // 
            this.button_ln.Location = new System.Drawing.Point(451, 65);
            this.button_ln.Name = "button_ln";
            this.button_ln.Size = new System.Drawing.Size(100, 31);
            this.button_ln.TabIndex = 9;
            this.button_ln.Text = "ln";
            this.button_ln.UseVisualStyleBackColor = true;
            this.button_ln.Click += new System.EventHandler(this.button_ln_Click);
            // 
            // button_multiply
            // 
            this.button_multiply.Location = new System.Drawing.Point(239, 102);
            this.button_multiply.Name = "button_multiply";
            this.button_multiply.Size = new System.Drawing.Size(100, 31);
            this.button_multiply.TabIndex = 10;
            this.button_multiply.Text = "*";
            this.button_multiply.UseVisualStyleBackColor = true;
            this.button_multiply.Click += new System.EventHandler(this.button_multiply_Click);
            // 
            // button_cos
            // 
            this.button_cos.Location = new System.Drawing.Point(345, 102);
            this.button_cos.Name = "button_cos";
            this.button_cos.Size = new System.Drawing.Size(100, 31);
            this.button_cos.TabIndex = 11;
            this.button_cos.Text = "cos";
            this.button_cos.UseVisualStyleBackColor = true;
            this.button_cos.Click += new System.EventHandler(this.button_cos_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.Location = new System.Drawing.Point(451, 102);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(100, 31);
            this.button_sqrt.TabIndex = 12;
            this.button_sqrt.Text = "sqrt";
            this.button_sqrt.UseVisualStyleBackColor = true;
            this.button_sqrt.Click += new System.EventHandler(this.button_sqrt_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 21);
            this.label3.TabIndex = 13;
            this.label3.Text = "Result:";
            // 
            // Label_result
            // 
            this.Label_result.AutoSize = true;
            this.Label_result.Location = new System.Drawing.Point(99, 86);
            this.Label_result.Name = "Label_result";
            this.Label_result.Size = new System.Drawing.Size(0, 21);
            this.Label_result.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 143);
            this.Controls.Add(this.Label_result);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_sqrt);
            this.Controls.Add(this.button_cos);
            this.Controls.Add(this.button_multiply);
            this.Controls.Add(this.button_ln);
            this.Controls.Add(this.button_tan);
            this.Controls.Add(this.button_sin);
            this.Controls.Add(this.button_divide);
            this.Controls.Add(this.button_minus);
            this.Controls.Add(this.button_plus);
            this.Controls.Add(this.TextBox_op2);
            this.Controls.Add(this.TextBox_op1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBox_op1;
        private System.Windows.Forms.TextBox TextBox_op2;
        private System.Windows.Forms.Button button_plus;
        private System.Windows.Forms.Button button_minus;
        private System.Windows.Forms.Button button_divide;
        private System.Windows.Forms.Button button_sin;
        private System.Windows.Forms.Button button_tan;
        private System.Windows.Forms.Button button_ln;
        private System.Windows.Forms.Button button_multiply;
        private System.Windows.Forms.Button button_cos;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Label_result;
    }
}

